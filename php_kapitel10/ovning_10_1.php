<!DOCTYPE html>
<html lang="sv">
    <head>
        <meta charset="utf-8">
        <title>Hur många jag har besökt sidan</title>
        <link rel="stylesheet" href="">
    </head>
    <body>
        <h1>Antal gånger jag har besökt sidan</h1>
        <?php
        session_start();

        if (empty($_SESSION["besok"]))
            $_SESSION["besok"] = 1;
        else
            $_SESSION["besok"] += 1;

        echo "<h2>{$_SESSION["besok"]}</h2>";
        ?>
    </body>
</html>
