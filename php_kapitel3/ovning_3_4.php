<?php
/**
 * Ett matematiskt räkneexempel
 *
 * Ett skript som skriver ut tal och respektive tals kvadrat från 0 till 50.
 *
 * @author     Karim Ryde <karye.webb@gmail.com>
 * @license    PHP CC
 */

/* Stäng av felmeddelanden */
ini_set('display_errors', 0);
?>
<!DOCTYPE html>
<html lang="sv">
    <head>
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" href="">
    </head>
    <body>
<?php
for ($i = 0; $i <= 50; $i++) {
    echo "<p>" . $i . " och kvadraten " . $i*$i . "</p>";
}
?>
    </body>
</html>
