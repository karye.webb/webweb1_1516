<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>Spara inlägg</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="../style.css">
</head>
<body>
    <?php
    // Ta emot data från formuläret
    $rubrik = $_POST['rubrik'];

    // Gör om radslut till taggen <br>
    $inlagg = $_POST['inlagg'];

    require_once('../include_konfig_db.php');

    // Anslut till databasen
    $conn = new mysqli($host, $user, $pass, $database);

    // Om någonting går fel. Avsluta med ett felmeddelande
    if ($conn->connect_error)
        die("Någonting blev fel: " . $conn->connect_error);

    // Vårt sql-kommando
    $sql = "INSERT INTO bloggen2
            (rubrik, inlagg) VALUES
            ('$rubrik', '$inlagg')";

    // Kör sql-kommandot
    $result = $conn->query($sql);

    // Gick det bra eller inte?
    if (!$result)
        die("Kunde inte spara inlägg: " . $conn->error);
    else
        echo "<h3>Ditt inlägg är registrerat</h3>";

    // Stäng ned databasanslutningen
    $conn->close();
    ?>
</body>
</html>
