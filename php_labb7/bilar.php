<?php

// Hämta databas login
require_once 'login.php';

// Vi kopplar upp oss mot vår databas
$conn = new mysqli($hostname, $user, $password, $database);

// Om någonting går fel. Avsluta och skriv ett felmeddelandet
if ($conn->connect_error) die($conn->connect_error);

// Vårt sql-kommando
$query = "SELECT * FROM bilar";

// Vi skickar sql-fråga till databasservern via vår databaskoppling
$result = $conn->query($query);

// Om sql-fråga kunde inte köras (det blev fel). Avsluta och skriv ett felmeddelande
if (!$result) die($result->error);
?>

<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>Hämta data från tabellen bilar</title>
    <link rel="stylesheet" href="bilar.css">
</head>
<body>

    <?php
    echo "<h1>Innehåll i tabellen bilar</h1>";
    echo "<table>";
    echo "<tr><th>Reg.nr.</th><th>Märke</th></tr>";
    while ($row = $result->fetch_assoc()) {
        echo "<tr>";
        echo "<td>" . $row['reg'] . "</td>";
        echo "<td>" . $row['marke'] . "</td>";
        echo "</tr>";
    }
    echo "</table>";

    // Stäng ned databasuppkoppling när vi är klara!
    $result->close();
    $conn->close();
    ?>

</body>
</html>
