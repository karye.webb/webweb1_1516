$(document).ready(function() {
    $("#plus").click(function(event) {
        $.post(
            "result.php", {
                tal1: $("#tal1").val(),
                tal2: $("#tal2").val(),
                op: "plus"
            },
            function(data) {
                $('#svar').val(data);
            }
        );
    });
    $("#minus").click(function(event) {
        $.post(
            "result.php", {
                tal1: $("#tal1").val(),
                tal2: $("#tal2").val(),
                op: "minus"
            },
            function(data) {
                $('#svar').val(data);
            }
        );
    });
});
