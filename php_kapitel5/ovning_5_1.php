<?php
/**
    * Sortering av namn
    * PHP version 5
    * @category   Enkel skriptsida
    * @package    Sortera 10 namn
    * @author     Karim Ryde <karye.webb@gmail.com>
    * @license    PHP CC
    * @link       http://twiggy/~ryde/..
    */

/* Stäng av felmeddelanden */
ini_set('display_errors', 'Off');
?>
<!DOCTYPE html>
<html lang="sv">
    <head>
        <meta charset="utf-8">
        <title>Sortera 10 namn</title>
    </head>
    <body>
<?php
    /* Tar emot data och kontrollerar att det är inte tomt */
    if (!empty($_REQUEST['namn'])) {
        echo "<h1>Namn sorterade i bokstavsordning</h1>";
        sort($_REQUEST['namn']);

        foreach ($_REQUEST['namn'] as $del) {
            echo "<p>$del</p>";
        }
    } else {
        echo "<h2>Ingen data mattades in!</h2>";
?>
        <h1>Mata in tio namn</h1>
        <!-- Formulär för att mata inte värden -->
        <form action="ovning_5_1.php" method="post">
            <label>Namn 1</label><input type="text" name="namn[]"><br>
            <label>Namn 2</label><input type="text" name="namn[]"><br>
            <label>Namn 3</label><input type="text" name="namn[]"><br>
            <label>Namn 4</label><input type="text" name="namn[]"><br>
            <label>Namn 5</label><input type="text" name="namn[]"><br>
            <label>Namn 6</label><input type="text" name="namn[]"><br>
            <label>Namn 7</label><input type="text" name="namn[]"><br>
            <label>Namn 8</label><input type="text" name="namn[]"><br>
            <label>Namn 9</label><input type="text" name="namn[]"><br>
            <label>Namn 10</label><input type="text" name="namn[]"><br>
            <input type="submit" value="Sortera">
        </form>
<?php
    }
?>
    </body>
</html>
