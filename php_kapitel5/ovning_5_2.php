<?php
/**
    * Sortering av namn
    * PHP version 5
    * @category   Enkel skriptsida
    * @package    Sortera 10 namn och skriv ut i en tabell
    * @author     Karim Ryde <karye.webb@gmail.com>
    * @license    PHP CC
    * @link       http://twiggy/~ryde/..
    */

/* Stäng av felmeddelanden */
ini_set('display_errors', 'Off');
?>
<!DOCTYPE html>
<html lang="sv">
    <head>
        <meta charset="utf-8">
        <title>Sortera 10 namn</title>
        <style>
            table {
                border: 1px solid;
                width: 100px;
            }
            td {
                padding: 10px;
            }
            .gra {
                background: #999;
            }
        </style>
    </head>
    <body>
<?php
    /* Tar emot data och kontrollerar att det är inte tomt */
    if (!empty($_REQUEST['namn'])) {
        echo "<h1>Namn sorterade i bokstavsordning</h1>";
        sort($_REQUEST['namn']);
        echo "<table>";
        foreach ($_REQUEST['namn'] as $pos => $del) {
            /* Ta reda på om $pos är jämnt eller udda */
            if ($pos % 2 == 0)
                echo "<tr><td class=\"vit\">$del</td></tr>";
            else
                echo "<tr><td class=\"gra\">$del</td></tr>";
        }
        echo "</table>";
    } else {
        echo "<h2>Ingen data mattades in!</h2>";
?>
        <h1>Mata in tio namn</h1>
        <!-- Formulär för att mata inte värden -->
        <form action="ovning_5_2.php" method="post">
            <label>Namn 1</label><input type="text" name="namn[]"><br>
            <label>Namn 2</label><input type="text" name="namn[]"><br>
            <label>Namn 3</label><input type="text" name="namn[]"><br>
            <label>Namn 4</label><input type="text" name="namn[]"><br>
            <label>Namn 5</label><input type="text" name="namn[]"><br>
            <label>Namn 6</label><input type="text" name="namn[]"><br>
            <label>Namn 7</label><input type="text" name="namn[]"><br>
            <label>Namn 8</label><input type="text" name="namn[]"><br>
            <label>Namn 9</label><input type="text" name="namn[]"><br>
            <label>Namn 10</label><input type="text" name="namn[]"><br>
            <input type="submit" value="Sortera">
        </form>
<?php
    }
?>
    </body>
</html>
