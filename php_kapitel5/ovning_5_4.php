<?php

$siffror = ["noll", "ett", "två", "tre", "fyra", "fem", "sex", "sju", "åtta", "nio"];

function talSvenska($tal1, $tal2) {
    global $siffror;

    $summa = $tal1 + $tal2;

    if ($summa > 9)
        echo "<p>$tal1 plus $tal2 = $summa</p>";
    else
        echo "<p>$siffror[$tal1] plus $siffror[$tal2] = $siffror[$summa]</p>";
}

?>

<!DOCTYPE html>
<html lang="sv">
    <head>
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" href="">
    </head>
    <body>
<?php
        echo "<p>" . talSvenska(7, 3) ."</p>";
        echo "<p>" . talSvenska(3, 4) ."</p>";
?>
    </body>
</html>
