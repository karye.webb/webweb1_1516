<?php

$siffror = ["noll", "ett", "två", "tre", "fyra", "fem", "sex", "sju", "åtta", "nio"];

function talSvenska($tal) {
    global $siffror;

    if ($tal > 9)
        return $tal;
    else
        return $siffror[$tal];
}

?>

<!DOCTYPE html>
<html lang="sv">
    <head>
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" href="">
    </head>
    <body>
<?php
        echo "<p>" . talSvenska(7) ."</p>";
        echo "<p>" . talSvenska(11) ."</p>";
?>
    </body>
</html>
