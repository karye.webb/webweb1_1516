<?php
/**
    * Lista alla filer i katalog
    * PHP version 5
    * @category   Enkel skriptsida
    * @package    Lista alla filer i vår katalog
    * @author     Karim Ryde <karye.webb@gmail.com>
    * @license    PHP CC
    * @link       http://twiggy/~ryde/..
    */

/* Stäng av felmeddelanden */
ini_set('display_errors', 'Off');
?>
<!DOCTYPE html>
<html lang="sv">
    <head>
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" href="">
    </head>
    <body>
        <?php
        $iterator = new FilesystemIterator(__DIR__, FilesystemIterator::CURRENT_AS_PATHNAME);
        foreach ($iterator as $fileinfo) {
            echo "<p>$fileinfo</p>";
        }

        // Lista innehållet i en katalog
        $path = realpath('/home/ryde/public_html');
        $iterator2 = new FilesystemIterator($path);
        foreach ($iterator2 as $fileinfo) {
            echo "<p>$fileinfo</p>";
        }
        ?>
    </body>
</html>
