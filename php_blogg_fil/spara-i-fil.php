<!doctype html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <title>Skriva till en fil</title>
    <link rel="stylesheet" href="styles.css">
</head>

<body>
<?php
$filnamn = "blogg.txt";
$file = fopen($filnamn, "a");
$texten = nl2br($_POST['inlagg'], false);
fwrite($file, "<div class=\"inlagg\"><h4>" . date('h:i Y/m/d') . "</h4><p>" . $texten . " </p></div>");
fclose($file);

echo "<h1>Inlägget registrerat!</h1>";
?>
</body>

</html>
