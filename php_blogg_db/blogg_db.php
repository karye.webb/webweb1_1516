<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>Min blogg!</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <?php
    // Databasuppgifter
    $hostname = 'localhost';
    $user = 'ryde_user';
    $password = 'ryde_pass';
    $database = 'ryde_db';

    // Anslut till databasen
    $conn = new mysqli($hostname, $user, $password, $database);

    // Om någonting går fel. Avsluta och skriv ett felmeddelandet
    if ($conn->connect_error)
        die("Kunde inte ansluta till databasen: " . $conn->connect_error);

    // Vårt sql-kommando
    $query = "SELECT * FROM bloggen;";

    // Kör sql
    $result = $conn->query($query);

    // Gick det bra? Om inte skriv ut felmeddelande
    if (!$result)
        die("Kunde inte hämta inlägg: " . $conn->error);

    echo "<h1>Min kära blogg</h1>";

    // Berätta hur många poster vi hittat som motsvara sökvillkoret
    echo "<p>Hittat " . $result->num_rows . " poster i tabellen</p>";

    // Skriv ut alla inlägg en efter en
    while ($row = $result->fetch_assoc()) {
        echo "<article>";
        echo "<h5>" . $row['tidstampel'] . "</h5>";
        echo "<p>" . $row['inlagg'] . "</p>";
        echo "</article>";
    }

    // Stäng ned databasanslutningen
    $result->free();
    $conn->close();
    ?>
</body>
</html>
