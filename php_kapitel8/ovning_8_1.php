<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <title>Skicka epost</title>
</head>

<body>
<?php
    // Kontrollera om vi får data eller inte
    if (!empty($_REQUEST['from_email']) &&
        !empty($_REQUEST['to_email']) &&
        !empty($_REQUEST['subject_email']) &&
        !empty($_REQUEST['body_email'])) {

        // Försöker skicka epost
        $subject_email = $_REQUEST['subject_email'];
        $to_email = $_REQUEST['to_email'];
        $from_email = "From: " . $_REQUEST['from_email'] . "\n";
        $body_email = $_REQUEST['body_email'];
        $felsvar = mail($to_email, $subject_email, $body_email, $from_email);

        // Felhantering av epost
        if ($felsvar) {
            echo "<p>Epost skickat till $to_email</p>";
        } else {
            echo "<p>Fel! Kunde inte skicka epost till $to_email</p>";
        }

    } else {
?>
        <form action="ovning_8_1.php" method="post">
            <fieldset>
                <legend>Epostformulär</legend>
                <label>Avsändaradress</label><input type="email" name="from_email"><br>
                <label>Motagaradresser</label><input type="text" name="to_email" placeholder="mail1, mail2..."><br>
                <label>Ärende</label><input type="text" name="subject_email"><br>
                <textarea name="body_email"></textarea><br>
                <input type="submit" value="Skicka epost">
            </fieldset>
        </form>
<?php
    }
?>

</body>

</html>
