<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8"/>
    <title></title>
    <link rel="stylesheet" href="">
</head>
<body>
<?php
    echo "\t<p>" . date("d F Y H:i") . "</p>\n";

    $namn = "Karim Ryde";
    $fodelseAr = 1964;
    $myndig = true;

    /* Skriv ut textsträng med också tolka variabler som finns med */
    echo "\t<p>Hej världen! Mitt namn är $namn och jag är född " . $fodelseAr . "</p>\n";
    /* När vi vill skriva ut allt, dvs inte tolka variabler */
    echo '<p>Hej världen! Mitt namn är $namn</p>';

    $tal1 = 37.76;
    $tal2 = 45.23;
    $summa = $tal1 + $tal2;
    echo "<p>$tal1 + $tal2 är lika med $summa</p>";

    $tal3 = "15 kr";
    $tal4 = "23 kr";
    $summa2 = $tal3 + $tal4; // Varning för att addera två strängar, kan bli lustigt resultat
    echo "<p>$tal3 + $tal4 = $summa2</p>";

    $summa3 = $tal3 . $tal4;
    echo "<p>$tal3 . $tal4 = $summa3</p>";

    $tal5 = "two";
    $tal6 = "nine";
    $summa4 = $tal5 + $tal6;
    echo gettype($summa4);
    echo "<p>$tal5 + $tal6 = $summa4</p>";

    $tal7 = 45.7;
    $antal = (string)$tal7;
    echo gettype($tal7);

    if (is_string($tal7))
        echo "true";
    else
        echo "false";

    /*
        Det finns massor med server-variabler som kan berätta mycket om miljön
        där skriptet körs.
    */
    echo "<p>Servers namn är {$_SERVER['SERVER_NAME']} och php-filen finns här {$_SERVER['PHP_SELF']}</p>";


?>
</body>
</html>
