<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8" />
    <title></title>
    <link rel="stylesheet" href="">
</head>

<body>
    <?php
    // Ta emot data
    $texten = $_REQUEST['texten'];
    $typ = $_REQUEST['typ'];

    if ($typ == 'V') {
        // Omvandla texten till versaler
        $texten = strtoupper($texten);
        echo "<p>Till versaler: $texten</p>";
    } else {
        // Omvandla texten till gemener
        $texten = strtolower($texten);
        echo "<p>Till gemener: $texten</p>";
    }
?>
</body>

</html>
