<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="">
</head>

<body>
<?php
    /* Kontroll: kommer in något? */
    if (isset($_REQUEST['personNr'])) {
        $personNr = $_REQUEST['personNr'];

        $fyraSista = substr($personNr, -4);

        echo "<p>Dina fyra sista siffror i $personNr är $fyraSista.</p>";
    } else {
        echo "<p>Du har inte matat in något!</p>";
    }
?>
</body>

</html>
