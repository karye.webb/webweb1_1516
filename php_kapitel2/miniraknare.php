<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="">
</head>

<body>
    <form action="miniraknare.php" method="post">
        <fieldset>
            <legend>Miniräknare</legend>
            <p>Ange två siffror och räknesätt</p>
            <input type="number" name="tal1">
            <input type="radio" name="raknasatt" value="plus" checked>+
            <input type="radio" name="raknasatt" value="minus">-
            <input type="radio" name="raknasatt" value="ganger">*
            <input type="radio" name="raknasatt" value="dividera">/
            <input type="number" name="tal2">
            <br>

            <input type="submit" value="Räkna">
        </fieldset>
    </form>
<?php
    /* Felhantering */
    $flagga = 0;

    if (empty($_REQUEST['tal1'])) {
        echo "<p>Du har inte matat in tal 1!</p>";
        $flagga = 1;
    }

    if (empty($_REQUEST['tal2'])) {
        echo "<p>Du har inte matat in tal 2!</p>";
        $flagga = 1;
    }

    if (empty($_REQUEST['raknasatt'])) {
        echo "<p>Du har inte angivit raknasätt!</p>";
        $flagga = 1;
    }

    if ($flagga == 0 ) {
        $tal1 = $_REQUEST['tal1'];
        $tal2 = $_REQUEST['tal2'];
        $raknasatt = $_REQUEST['raknasatt'];

        switch ($raknasatt) {
            case "plus":
                $total = $tal1 + $tal2;
                break;
            case "minus":
                $total = $tal1 - $tal2;
                break;
            case "ganger":
                $total = $tal1 * tal2;
                break;
            case "dividera":
                $total = $tal1 / $tal2;
                break;
        }
        echo "<p>Resultatet är =$total</p>";
    }
?>
</body>

</html>
