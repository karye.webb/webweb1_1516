<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="">
</head>

<body>
<?
    /* Kontroll: kommer in något? */
    $flagga = 0;

    if (empty($_REQUEST['texten'])) {
        echo "<p>Du har inte matat in texten!</p>";
        $flagga = 1;
    }

    if (empty($_REQUEST['ord1'])) {
        echo "<p>Du har inte matat in sökordet!</p>";
        $flagga = 1;
    }

    if (empty($_REQUEST['ord2'])) {
        echo "<p>Du har inte matat in ersättningsordet!</p>";
        $flagga = 1;
    }

    if ($flagga == 0 ) {
        $texten = $_REQUEST['texten'];
        $ord1 = $_REQUEST['ord1'];
        $ord2 = $_REQUEST['ord2'];

        $nyText = str_replace($ord1, $ord2, $texten);
        echo "<p>Den nya texten blir:$nyText</p>";
    }
?>
</body>

</html>
