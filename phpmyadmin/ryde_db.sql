-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Värd: localhost
-- Skapad: 08 apr 2016 kl 10:37
-- Serverversion: 5.5.47-MariaDB-1ubuntu0.14.04.1
-- PHP-version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databas: `ryde_db`
--

-- --------------------------------------------------------

--
-- Tabellstruktur `agare2`
--

CREATE TABLE IF NOT EXISTS `agare2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fnamn` varchar(30) NOT NULL,
  `enamn` varchar(30) NOT NULL,
  `mobil` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumpning av Data i tabell `agare2`
--

INSERT INTO `agare2` (`id`, `fnamn`, `enamn`, `mobil`) VALUES
(1, ' Agneta', ' Andersson', '070-2355642'),
(2, ' Kalle', ' Karlsson', ' 070-8959599'),
(3, ' Johan', ' Sundborn', ' 070-3565656'),
(4, ' Markus', ' Svegfors', ' 070-6862656'),
(5, ' Anna', ' Dahlén', ' 070-2132121'),
(6, ' Helene', ' Petersson', ' 070-7878788'),
(7, ' Catti', ' Holmberg', ' 070-6565656'),
(8, ' Märeta', ' Svensson', ' 070-3232323');

-- --------------------------------------------------------

--
-- Tabellstruktur `bilar`
--

CREATE TABLE IF NOT EXISTS `bilar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reg` varchar(10) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL,
  `marke` varchar(50) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL,
  `modell` varchar(50) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL,
  `arsmodell` int(11) DEFAULT NULL,
  `pris` int(11) DEFAULT NULL,
  `agare` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `agare` (`agare`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumpning av Data i tabell `bilar`
--

INSERT INTO `bilar` (`id`, `reg`, `marke`, `modell`, `arsmodell`, `pris`, `agare`) VALUES
(1, 'ABC123', 'Saab', '9-5', 2003, 130000, 1),
(3, 'DEF123', 'Volvo', 'S80', 2002, 140000, 2),
(4, 'DEF456', 'Toyota', 'Carina II', 1998, 30000, 2),
(5, 'GHI123', 'Mazda', '626', 2001, 80000, 3),
(6, 'JKL123', 'Audi', 'A8', 2001, 150000, 5),
(7, 'MNO123', 'BMW', '323', 1998, 60000, 5),
(8, 'PQR123', 'Ford', 'Mondeo', 2001, 90000, 4),
(9, 'STU123', 'Volvo', '740', 1987, 35000, 5),
(10, 'VYX123', 'Volkswagen', 'Golf', 1988, 40000, 5),
(11, 'XXX333', 'Nissan', 'Primera', 2003, 100000, NULL);

-- --------------------------------------------------------

--
-- Tabellstruktur `hastar2`
--

CREATE TABLE IF NOT EXISTS `hastar2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hast` varchar(30) NOT NULL,
  `alder` int(11) NOT NULL,
  `stall_id` int(11) NOT NULL,
  `agare_id` int(11) NOT NULL,
  `kon` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumpning av Data i tabell `hastar2`
--

INSERT INTO `hastar2` (`id`, `hast`, `alder`, `stall_id`, `agare_id`, `kon`) VALUES
(1, 'Amigo ox', 5, 1, 1, ' Sto'),
(2, 'Ardeeza Bint Parona ox', 6, 2, 10, ' Sto'),
(3, 'Arkusz ox ', 12, 3, 3, ' Sto'),
(4, 'Ekbäckens Nypon', 5, 2, 6, ' Valack'),
(5, 'Ekbäckens Savoy RWM 32', 6, 6, 7, ' Valack'),
(6, 'Farina', 12, 6, 10, ' Sto'),
(7, 'Nachir ox', 5, 5, 9, ' Valack'),
(8, 'Nafud Shahina ox', 6, 5, 9, ' Hingst'),
(9, 'Nickolina Chayn', 12, 3, 9, ' Sto');

-- --------------------------------------------------------

--
-- Tabellstruktur `personer`
--

CREATE TABLE IF NOT EXISTS `personer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fnamn` char(50) DEFAULT NULL,
  `enamn` char(50) DEFAULT NULL,
  `fodelsedatum` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumpning av Data i tabell `personer`
--

INSERT INTO `personer` (`id`, `fnamn`, `enamn`, `fodelsedatum`) VALUES
(1, 'Kalle', 'Anka', '1934-07-09'),
(2, 'Kajsa', 'Anka', '1937-09-01'),
(3, 'Knatte', 'Anka', '1937-10-17'),
(4, 'Tjatte', 'Anka', '1937-10-17'),
(5, 'Fnatte', 'Anka', '1937-10-17'),
(6, 'Knase', 'Anka', '1964-08-02'),
(7, 'Alexander', 'Lukas', '1948-01-01');

-- --------------------------------------------------------

--
-- Tabellstruktur `spel`
--

CREATE TABLE IF NOT EXISTS `spel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namn` char(20) DEFAULT NULL,
  `land` char(20) DEFAULT NULL,
  `tillverkare` char(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumpning av Data i tabell `spel`
--

INSERT INTO `spel` (`id`, `namn`, `land`, `tillverkare`) VALUES
(1, 'Battlefield 1942', 'Sverige', 'Dice'),
(2, 'Battlefield Vietnam', 'Sverige', 'Dice'),
(3, 'Bad Company', 'Sverige', 'Dice');

-- --------------------------------------------------------

--
-- Tabellstruktur `stall2`
--

CREATE TABLE IF NOT EXISTS `stall2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stall` varchar(30) NOT NULL,
  `platser` int(11) NOT NULL,
  `tranare_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumpning av Data i tabell `stall2`
--

INSERT INTO `stall2` (`id`, `stall`, `platser`, `tranare_id`) VALUES
(1, 'Landbacken', 6, 1),
(2, 'Nacka Ridsport', 12, 2),
(3, 'Lilla Hagen', 10, 3),
(4, 'Bondens Häst', 23, 4),
(5, 'Travbanan Väst', 15, 5),
(6, 'Ridhästen', 15, 2),
(7, 'Solna Ridhäst', 21, 1);

-- --------------------------------------------------------

--
-- Tabellstruktur `test`
--

CREATE TABLE IF NOT EXISTS `test` (
  `namn` char(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur `test2`
--

CREATE TABLE IF NOT EXISTS `test2` (
  `namn` char(20) DEFAULT NULL,
  `enamn` char(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumpning av Data i tabell `test2`
--

INSERT INTO `test2` (`namn`, `enamn`) VALUES
('Kalle', 'Anka'),
('Kajsa', 'Anka'),
('Joakim', 'Anka');

-- --------------------------------------------------------

--
-- Tabellstruktur `tranare2`
--

CREATE TABLE IF NOT EXISTS `tranare2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fnamn` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumpning av Data i tabell `tranare2`
--

INSERT INTO `tranare2` (`id`, `fnamn`) VALUES
(1, 'Gunnar'),
(2, 'Kalle'),
(3, 'Berit'),
(4, 'Annelie'),
(5, 'Bertil');

--
-- Restriktioner för dumpade tabeller
--

--
-- Restriktioner för tabell `bilar`
--
ALTER TABLE `bilar`
  ADD CONSTRAINT `bilar_ibfk_1` FOREIGN KEY (`agare`) REFERENCES `personer` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
